import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import { uniqueId, keyBy } from 'lodash';
import * as actions from '../actions';

const modes = handleActions({
  [actions.setModes](state, { payload }) {
    const newState = Object.entries(payload)
      .reduce((acc, [key, obj]) => {
        acc[key] = {
          ...obj,
          id: uniqueId(key),
          label: key.toUpperCase().replace('MODE', ''),
          value: key.toLowerCase()
        };
        return acc;
      }, {});

    return newState;
  },
}, {});

const currentMode = handleActions({
  [actions.setCurrentMode](state, { payload }) {
    return payload;
  }
}, {});

const leaderBoard = handleActions({
  [actions.setLeaderBoard](state, { payload }) {
    return {
      ...state,
      winners: payload
    };
  }
}, {});

const player = handleActions({
  [actions.setPlayerName](state, { payload }) {
    return {
      ...state,
      name: payload
    };
  }
}, {});

const board = handleActions({
  [actions.setCells](state, { payload }) {
    const cells = payload.reduce((acc) => [...acc, { id: uniqueId(), state: 'default' }], []);
    return {
      ...state,
      cellsById: keyBy(cells, 'id')
    };
  },
  [actions.resetCells](state) {
    const cells = Object.values(state.cellsById)
      .map((c) => ({ ...c, state: 'default' }));

    return {
      ...state,
      cellsById: keyBy(cells, 'id')
    };
  },
  [actions.setCellState](state, { payload }) {
    const cell = state.cellsById[payload.id];

    if (cell.state === 'clicked' && payload.state === 'missed') return state;

    return {
      ...state,
      cellsById: {
        ...state.cellsById,
        [cell.id]: { ...cell, state: payload.state }
      }
    };
  }
}, {});

const game = handleActions({
  [actions.setGameState](state, { payload }) {
    return {
      ...state,
      state: payload
    };
  },
  [actions.setGameWinner](state, { payload }) {
    return {
      ...state,
      winner: payload
    };
  }
}, {});

export default combineReducers({
  modes,
  leaderBoard,
  currentMode,
  player,
  board,
  game
});
