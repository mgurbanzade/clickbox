export default {
  settingsEndpoint: () => `${process.env.API_URL}/game-settings`,
  winnersEndpoint: () => `${process.env.API_URL}/winners`,
};
