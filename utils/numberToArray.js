export default (maxNum) => {
  const arr = [];

  for (let i = 0; i < maxNum; i += 1) {
    arr.push(i);
  }

  return arr;
};
