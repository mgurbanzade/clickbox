import { connect } from 'react-redux';
import * as actionCreators from '../actions';

export default (props) => (Component) => connect(props, actionCreators)(Component);
