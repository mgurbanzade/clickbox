export default () => ({
  modes: {},
  leaderBoard: {
    winners: []
  },
  currentMode: null,
  player: {
    name: ''
  },
  board: {
    cellsById: {}
  },
  game: {
    state: 'loading',
    winner: {
      name: '',
      score: 0
    }
  }
});
