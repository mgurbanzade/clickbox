import React from 'react';
import connect from '../utils/connect';

const mapStateToProps = (state) => ({
  cellsById: state.board.cellsById,
  game: state.game
});

const Cell = ({
  id,
  size,
  cellsById,
  setCellState,
  game
}) => {
  const currentCell = cellsById[id];
  const handleClick = () => {
    if (game.state === 'stopped' || currentCell.state !== 'active') return;
    setCellState({ id, state: 'clicked' });
  };
  const stateColorDispatcher = {
    default: '#FFFFFF',
    active: '#17A2B8',
    clicked: '#28A745',
    missed: '#DC3545',
  };

  const cellStyles = {
    width: size,
    height: size,
    border: '1px solid #eee',
    backgroundColor: stateColorDispatcher[currentCell.state],
  };

  return (
    <div
      className="cell"
      role="cell"
      onClick={() => handleClick()}
      style={cellStyles}
    ></div>
  );
};

export default connect(mapStateToProps)(Cell);
