import React from 'react';
import { Form, Button } from 'react-bootstrap';
import connect from '../utils/connect';
import ModeSelect from './ModeSelect';

const mapStateToProps = (state) => ({
  modes: Object.values(state.modes),
  currentMode: state.currentMode,
  player: state.player,
  game: state.game
});

const Navbar = (props) => {
  const {
    currentMode, game, player: { name }, setPlayerName, setGameState, resetCells
  } = props;
  const btnIsDisabled = game.state === 'started' || currentMode === null || name.trim().length === 0;
  const btnTitle = game.state === 'stopped' ? 'Play again!' : 'Play';
  const handleClick = () => {
    if (game.state === 'stopped') resetCells();
    setGameState('started');
  };

  return (
    <nav className="navbar row flex-nowrap" style={{ marginBottom: 60 }}>
      <ModeSelect />
      <Form.Control
        onChange={(e) => setPlayerName(e.target.value)}
        className="col-md-4"
        type="text"
        placeholder="Enter your name"
        disabled={game.state === 'started'}
      />
      <Button
        variant="primary"
        onClick={() => handleClick()}
        disabled={btnIsDisabled}
      >
        {btnTitle}
      </Button>
    </nav>
  );
};

export default connect(mapStateToProps)(Navbar);
