import React from 'react';
import { ListGroup, Container } from 'react-bootstrap';
import connect from '../utils/connect';

const mapStateToProps = ({ leaderBoard }) => ({ leaderBoard });

const LeaderBoard = ({ leaderBoard: { winners } }) => {
  const winnersMapped = winners.map((winner) => (
      <ListGroup.Item key={winner.id}>
        <p>{winner.winner}</p>
        <em>{winner.date}</em>
      </ListGroup.Item>
  ));

  return (
    <Container>
      <h2 className="text-primary text-center mb-4 mt-4">Winners</h2>
      <ListGroup>{winnersMapped}</ListGroup>
    </Container>
  );
};

export default connect(mapStateToProps)(LeaderBoard);
