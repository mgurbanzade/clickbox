import React, { useEffect, useState } from 'react';
import { format } from 'date-fns';
import connect from '../utils/connect';
import Cell from './Cell';

const mapStateToProps = ({
  board: { cellsById, activeCellId }, currentMode, game, player
}) => ({
  cells: Object.values(cellsById),
  activeCellId,
  currentMode,
  game,
  playerName: player.name
});

const Board = ({
  cells,
  currentMode,
  setCellState,
  game,
  setGameState,
  setGameWinner,
  playerName,
  setLeaderBoardWinner,
}) => {
  const [prevCellId, setPrevCellId] = useState(0);
  const boardSize = 420;
  const cellElements = cells.map((c) => (
    <Cell key={c.id} id={c.id} size={boardSize / currentMode.field} />
  ));

  const checkWinner = () => {
    const clickedCells = cells.filter((c) => c.state === 'clicked').length;
    const missedCells = cells.filter((c) => c.state === 'missed').length;
    const playerScore = clickedCells * 10;
    const cpuScore = missedCells * 10;
    const halfCellsCount = cells.length / 2;

    const winnerDispatcher = {
      [playerScore]: playerName,
      [cpuScore]: 'Computer',
    };

    if (clickedCells > halfCellsCount || missedCells > halfCellsCount) {
      setGameState('stopped');
      setPrevCellId(0);
      const winner = {
        name: winnerDispatcher[Math.max(playerScore, cpuScore)],
        score: Math.max(playerScore, cpuScore),
      };
      setGameWinner(winner);
      setLeaderBoardWinner({
        winner: winner.name,
        date: format(Date.now(), 'HH:mm; dd MMM yyyy'),
      });
    }
  };

  useEffect(() => {
    if (currentMode === null || game.state !== 'started') {
      setPrevCellId(0);
      return;
    }

    const setCell = () => {
      checkWinner();
      const availableCells = cells.filter((c) => c.state === 'default');
      const randomIndex = Math.floor(Math.random() * availableCells.length);
      const cell = availableCells[randomIndex];
      if (cell) setCellState({ id: cell.id, state: 'active' });
      if (prevCellId !== 0) setCellState({ id: prevCellId, state: 'missed' });
      if (cell) setPrevCellId(cell.id);
    };

    const interval = setInterval(setCell, currentMode.delay);

    return () => clearInterval(interval);
  });

  const boardStyles = {
    width: boardSize,
    margin: '0 auto',
    border: currentMode ? '1px solid #eee' : 0,
    boxSizing: 'content-box',
  };

  return (
    <div className="board row" style={boardStyles}>
      {cellElements}
    </div>
  );
};

export default connect(mapStateToProps)(Board);
