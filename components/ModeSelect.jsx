import React from 'react';
import Select from 'react-select';
import connect from '../utils/connect';
import numberToArray from '../utils/numberToArray';

const mapStateToProps = (state) => ({
  modes: Object.values(state.modes),
  game: state.game
});

const ModeSelect = (props) => {
  const { setCurrentMode, setCells, game } = props;

  const handleChange = (mode) => {
    setCurrentMode(mode);
    setCells(numberToArray(mode.field ** 2));
  };

  const defaultStyles = {
    control: (styles) => ({ ...styles, width: 200 }),
  };

  return (
    <Select
      instanceId="1"
      options={props.modes}
      styles={defaultStyles}
      placeholder="Pick a game mode"
      onChange={(mode) => handleChange(mode)}
      isDisabled={game.state === 'started'}
    />
  );
};

export default connect(mapStateToProps)(ModeSelect);
