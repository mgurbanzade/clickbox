import React, { useEffect } from 'react';
import axios from 'axios';
import { Row, Col } from 'react-bootstrap';
import routes from '../utils/routes';
import connect from '../utils/connect';
import Game from './Game';
import LeaderBoard from './LeaderBoard';

const App = (props) => {
  const fetchInitialState = async () => {
    const { setModes, setLeaderBoard } = props;
    const settingsRoute = routes.settingsEndpoint();
    const winnersRoute = routes.winnersEndpoint();

    try {
      const settings = await axios.get(settingsRoute);
      const leaderBoard = await axios.get(winnersRoute);

      setModes(settings.data);
      setLeaderBoard(leaderBoard.data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    fetchInitialState();
  }, []);

  return (
    <Row>
      <Col md={12} lg={6}>
        <Game />
      </Col>
      <Col md={12} lg={6}>
        <LeaderBoard />
      </Col>
    </Row>
  );
};

export default connect()(App);
