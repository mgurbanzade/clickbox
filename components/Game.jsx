import React from 'react';
import { Container } from 'react-bootstrap';
import Navbar from './Navbar';
import Logger from './Logger';
import Board from './Board';


const Game = () => (
    <Container>
      <h2 className="text-primary text-center mb-4 mt-4">ClickBox Game</h2>
      <Navbar />
      <Logger />
      <Board />
    </Container>
);

export default Game;
