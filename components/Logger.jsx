import React from 'react';
import connect from '../utils/connect';

const mapStateToProps = (state) => ({ game: state.game });

const Logger = ({ game: { state, winner } }) => {
  const msg = state === 'stopped' ? (
      <h3 className="text-center mb-5">
        {`${winner.name} won! Score: ${winner.score}`}
      </h3>
  ) : null;

  return (msg);
};

export default connect(mapStateToProps)(Logger);
