import React from 'react';
import Head from 'next/head';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import Layout from '../components/Layout';
import App from '../components/App';
import reducers from '../reducers';
import initializeState from '../utils/initialState';

const middleware = [applyMiddleware(thunk)];
const store = createStore(reducers, initializeState(), compose(...middleware));

export default function Home() {
  return (
    <div className="clickbox-game">
      <Head>
        <title>ClickBox Game</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossOrigin="anonymous"
        />
      </Head>
      <Layout>
        <Provider store={store}>
          <App />
        </Provider>
      </Layout>
    </div>
  );
}
