import { createAction } from 'redux-actions';
import axios from 'axios';
import routes from '../utils/routes';

export const setModes = createAction('MODES_SET');
export const setLeaderBoard = createAction('LEADERBOARD_SET');
export const setCurrentMode = createAction('CURRENT_MODE_SET');
export const setPlayerName = createAction('PLAYER_SET_NAME');
export const setCells = createAction('BOARD_SET_CELLS');
export const resetCells = createAction('BOARD_RESET_CELLS');
export const setCellState = createAction('BOARD_SET_CELL_STATE');
export const setActiveCell = createAction('BOARD_SET_ACTIVE_CELL');
export const setGameState = createAction('GAME_SET_STATE');
export const setGameWinner = createAction('GAME_SET_WINNER');

export const setLeaderBoardWinner = (payload) => async (dispatch) => {
  const path = routes.winnersEndpoint();
  try {
    const res = await axios.post(path, payload);
    dispatch(setLeaderBoard(res.data));
    console.log(res);
  } catch (e) {
    console.log(e);
  }
};
